import React, { useEffect, useState } from 'react'

function Practice() {
    const [x,setx]=useState(0)
    const [y,setY]=useState(0)
    const [toggle,settoggle]=useState(true)
    const position=(e)=>{
        console.log('mouseevent')
        setx(e.clientX)
        setY(e.clientY)
    }
    useEffect(()=>{
        window.addEventListener('mouseover',position)
        return()=>{
            console.log('component  is  unmount ')
            window.removeEventListener('mouseover',position)
        }
    })
    return (
        <div>
            {/* <h1>Hooks Y-{y} </h1> */}
            <button onClick={()=>settoggle(!toggle)}>Toggle</button>
            {
                toggle && 
                <h1>Hooks X-{x} y-{y} </h1>
            }

        </div>
    )
}

export default Practice
