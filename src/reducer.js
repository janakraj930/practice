import React,{useReducer} from 'react'
const initialstate={
    Counter:0
}
const reducer=(state,action)=>{
       switch(action.type){
           case 'Increament':
               return {Counter:state.Counter  + action.value}
               
            case 'Decreament':
                return {Counter:state.Counter - action.value}
            
            case 'Reset':
                return initialstate
            
            default:
                return state
       }


}
function Reducer() {
  const [currrent,dispatch]=  useReducer(reducer,initialstate)
    return (
        <div>
            <h1>Count ---{currrent.Counter}</h1>
            <h1>
                <button onClick={()=>dispatch({type:'Increament',value:5})}> Increament5</button> 
                 <button onClick={()=>dispatch({type:'Decreament',value:5})}>Decreament5</button>
                <button onClick={()=>dispatch({type:'Increament',value:1})}> Increament</button>
                <button onClick={()=>dispatch({type:'Decreament',value:1})}>Decreament</button>
                <button onClick={()=>dispatch({type:'Reset'})}>Reset</button>
            </h1>
        </div>
    )
}

export default Reducer
