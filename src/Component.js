import React, { useEffect, useState } from 'react'
import axios from 'axios'
function Component() {
    const [photo,setPhoto]=useState([])
    useEffect(()=>{
        axios.get(`https://jsonplaceholder.typicode.com/posts`)
        .then((data)=>{
            console.log(data)
            setPhoto(data.data)
        })
        .catch((err)=>{
            console.log(err)
        })
        
    })
    return (
        <div>
            <ul>
            {
                photo.map((item)=><li key={item.id}>
                    {item.title}
                    
                </li>)
            }
            </ul>
        </div>
    )
}

export default Component
